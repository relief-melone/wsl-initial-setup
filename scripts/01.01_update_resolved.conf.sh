#!/bin/bash

printf "\n\
==========================\n\
  Updating resolve.conf \n\
==========================\n\
"

BASEDIR=$(dirname $0)

cp $BASEDIR/../etc/systemd/resolved.conf /etc/systemd/resolved.conf

if [[ -z $http_proxy ]]; then
  systemctl restart systemd-resolved
else
  read -p "you are using a proxy. if you haven't restarted your system yet please do it first. do you want to continue? (y/N): " cont
  if [[ $cont  != 'y' ]]; then
    exit 1
  fi
fi
    
apt-get update

# Forward all localhost ports to default interface
addLine="net.ipv4.conf.all.route_localnet = 1"

grep -qxF "$addLine" /etc/sysctl.conf || echo $addLine >> /etc/sysctl.conf

# Apply the change
sudo sysctl -p /etc/sysctl.conf