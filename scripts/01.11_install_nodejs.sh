#!/bin/bash

export NVM_VERSION=v0.39.2
printf "\n\
==========================\n\
     INSTALLING NODEJS\n\
==========================\n\

NVM VERSION:
"

if [[ !$(command -v curl) ]]; then
  apt-get install -y curl
fi

curl -o https://raw.githubusercontent.com/nvm-sh/nvm/${NVM_VERSION}/install.sh | bash
sudo -u $SUDO_USER curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/${NVM_VERSION}/install.sh | sudo -u $SUDO_USER bash

export NVM_DIR=/home/$SUDO_USER/.nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

nvm install stable
nvm use stable
