#!/bin/bash
printf "\n\
==========================\n\
INSTALLING POWERLINE FONTS\n\
==========================\n\
"

add-apt-repository -y ppa:longsleep/golang-backports
apt-get update -y

# Install Go and Powerline fonts
apt-get install -y golang-go
apt-get install -y fonts-powerline

# Get Powerline-go for root and user
if [[ -n $SUDO_USER ]]; then
    sudo -u $SUDO_USER go install github.com/justjanne/powerline-go@latest
fi
go install github.com/justjanne/powerline-go@latest
