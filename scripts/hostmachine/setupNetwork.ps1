# The script solves multiple networking issues with access from WSL to host and vice versa
# and enables the connection from WSL2 to px proxy
#
# Prerequisites:
# - Windows admin rights
# - WSL
#
# Setup:
# Start Powershell as admin and run the command:
# Powershell.exe -executionpolicy bypass -File wsl-update.ps1
#
# Usage:
# Access WSL services from host via wsl e.g. https://wsl
# and host services from WSL via wsl.host e.g. http://wsl.host:3128


# Replace is a workaround for powershell builds older than V5.1 Build 19041 where output was in unicode instead of utf-8 resulting in extra spaces

# $wsl_distro = wsl /bin/bash -c "/mnt/c/Windows/system32/wsl.exe -l -v | head -n 2 | tail -n 1 | cut -d ' ' -f 2" -replace " ", ""
$wsl_distro = Read-Host -Prompt "Please enter the name of your wsl distro: "
$dns_config_path = "$PSScriptRoot\..\..\config\dns.conf"

if (Test-Path $dns_config_path){
    Write-Host "DNS Settings detected. If you want to reset delete $dns_config_path"
    $wsl_dns_ip = Get-Content $dns_config_path
} else {
    $wsl_dns_ip = Read-Host -Prompt 'Please enter dns ip'

    $save_dns = Read-Host -Prompt "Do you want to save that dns for the future? (delete config/dns.conf if you don't want to use the dns anymore) (y/n): "
    If ( $save_dns -eq "y" ){
        echo $wsl_dns_ip > $dns_config_path
    }
}

# $wsl_dns_ip="160.50.250.20"

cd $PSScriptRoot\..\..\
$root_path_wsl = wsl --exec "pwd"


$host_loopback_ip="127.0.0.1"
$px_port=3128
$wsl_host_ip="169.254.254.1"
$wsl_ip="169.254.254.2"
$wsl_broadcast="169.254.254.255"

$wsl_http_proxy="http://${wsl_host_ip}:3128/"
$wsl_https_proxy="http://${wsl_host_ip}:3128/"
$wsl_no_proxy="localhost,127.0.0.1,wsl.host,${wsl_host_ip}"

Write-Host """


########################
### SETTINGS SUMMARY ###
########################

WSL DISTRO:                 $wsl_distro
WSL SETUP FOLDER:           $root_path_wsl
IP SETTINGS:
    HOST:                   $wsl_host_ip
    WSL:                    $wsl_ip
    BROADCAST:              $wsl_broadcast
    DNS:                    $wsl_dns_ip
    HTTP_PROXY              $wsl_http_proxy
    HTTPS_PROXY             $wsl_https_proxy
    NO_PROXY                $wsl_no_proxy


    
"""

$settings_checked = Read-Host -Prompt 'Are the settings correct (y/n): '
if( $settings_checked -ne "y" ){
    exit
}

Try
{

    ##### Checking Pre-Requisites #####

    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    if (!$currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
        throw "Prerequisite not met: running as administrator. Please run as administrator."
    }

    if (Get-NetAdapter | Where-Object {$_.InterfaceDescription -match "Cisco AnyConnect"} | Where-Object {$_.Status -eq "Up"}) {
        throw "Prerequisite not met: VPN connection disabled. Please disconnect from VPN and run again."
    }

    Write-Host """
    #################################
    ####### WSL2 Network Setup ######
    #################################
    """

    wsl -d $wsl_distro -u root bash -c "dpkg -s net-tools 2>/dev/null >/dev/null || (echo 'Installing net-tools ...' && dpkg -i $root_path_wsl/packages/net-tools/net-tools_1.60+git20180626.aebd88e-1ubuntu1_amd64.deb && echo)"

    Write-Host "Stop automatic generation of wsl2's /etc/hosts and /etc/resolv.conf files"

    $wsl_config_exist = wsl.exe -d $wsl_distro bash -c "test -f '/etc/wsl.conf' && echo true"

    If ([string]::IsNullOrEmpty($wsl_config_exist)){
        wsl -d $wsl_distro -u root bash -c "echo '[network]' > /etc/wsl.conf"
        wsl -d $wsl_distro -u root bash -c "echo 'generateResolvConf  = false' >> /etc/wsl.conf"
        wsl -d $wsl_distro -u root bash -c "echo 'generateHosts = false' >> /etc/wsl.conf"

        wsl --shutdown
    }

    $wsl_etc_resolv_symlink = wsl.exe -d $wsl_distro bash -c "[ -L /etc/resolv.conf ] && echo 'Default symbolic link /etc/resolv.conf found'"
    
    If (![string]::IsNullOrEmpty($wsl_etc_resolv_symlink)){
        Write-Host "Set wsl dns ip in hosts's /etc/resolv.conf"
        wsl -d $wsl_distro -u root bash -c "rm /etc/resolv.conf"
        wsl -d $wsl_distro -u root bash -c "echo -e 'nameserver $wsl_dns_ip' >> /etc/resolv.conf"
    }

    Write-Host "Set wsl dns ip in wsl's /etc/hosts"

    $wsl_etc_hosts_updated = wsl.exe -d $wsl_distro grep 'wsl.host' /etc/hosts

    If ([string]::IsNullOrEmpty($wsl_etc_hosts_updated)){
        wsl -d $wsl_distro -u root bash -c "echo '$wsl_host_ip wsl.host' >> /etc/hosts"
    } Else {
        wsl -d $wsl_distro -u root bash -c "sed -i 's/.*wsl.host/$wsl_host_ip wsl.host/g' /etc/hosts"
    }
    
    Write-Host "Attach static ip to the wsl2 interface"

    wsl -d $wsl_distro -u root ip addr change $wsl_ip/24 broadcast $wsl_broadcast dev eth0 label eth0:1

    Write-Host "Set wsl host ip as default gateway"

    wsl -d $wsl_distro -u root route del default
    wsl -d $wsl_distro -u root route add default gw $wsl_host_ip

    # Write-Host """
    # #################################
    # #####       Proxy Setup     #####
    # #################################
    # """

    # $proxy_config_exist = wsl.exe -d $wsl_distro grep 'https_proxy' ~/.bashrc

    # If ([string]::IsNullOrEmpty($proxy_config_exist)){
    #     Write-Host "no proxy config found. Setting proxy..."
    #     Write-Host "setting ~/.bashrc"

    #     wsl.exe -d $wsl_distro bash -c "
    #         echo -e 'export http_proxy=${wsl_http_proxy}' >> ~/.bashrc && \
    #         echo -e 'export https_proxy=${wsl_https_proxy}' >> ~/.bashrc && \
    #         echo -e 'export HTTP_PROXY=${wsl_http_proxy}' >> ~/.bashrc && \
    #         echo -e 'export HTTPS_PROXY=${wsl_https_proxy}' >> ~/.bashrc && \
    #         echo -e 'export no_proxy=${wsl_no_proxy}' >> ~/.bashrc && \
    #         echo -e 'export NO_PROXY=${wsl_no_proxy}' >> ~/.bashrc"

    #     Write-Host "setting /etc/environment"
    #     wsl.exe -d $wsl_distro -u root bash -c "
    #         echo -e 'http_proxy=${wsl_http_proxy}' >> /etc/environment && \
    #         echo -e 'https_proxy=${wsl_https_proxy}' >> /etc/environment && \
    #         echo -e 'HTTP_PROXY=${wsl_http_proxy}' >> /etc/environment && \
    #         echo -e 'HTTPS_PROXY=${wsl_https_proxy}' >> /etc/environment && \
    #         echo -e 'no_proxy=${wsl_no_proxy}' >> /etc/environment && \
    #         echo -e 'NO_PROXY=${wsl_no_proxy}' >> /etc/environment && \
    #         echo -e 'Acquire::http::proxy \`"${wsl_http_proxy}\`";' > /etc/apt/apt.conf.d/proxy.conf && \
    #         echo -e 'Acquire::https::proxy \`"${wsl_http_proxy}\`";' >> /etc/apt/apt.conf.d/proxy.conf"
    # }

    # DISABLE UBUNTU PUBLIC SOURCES

    # Write-Host "Disable default Ubuntu public sources in wsl's /etc/apt/sources.list"   
    # wsl.exe -d $wsl_distro -u root bash -c "sed -i '/^deb http:\/\/archive\.ubuntu\.com\/ubuntu\/.*/s/^/# /g' /etc/apt/sources.list"

    Write-Host """
    #################################
    ##### Windows Network Setup #####
    #################################
    """

    if ((Get-NetAdapter -Name "vEthernet (WSL)" | Get-NetIPAddress).IPv4Address | Where-Object {$_ -Match $wsl_host_ip}){
        Write-Host "Static ip already attached to the windows virtual wsl2 interface"
    } Else {
        Write-Host "Attach static ip to the windows virtual wsl2 interface"
        netsh interface ip add address "vEthernet (WSL)" $wsl_host_ip 255.255.255.0
    }

    Write-Host "Force static ip as default ip"
    Set-NetIPAddress -IPAddress $wsl_host_ip -SkipAsSource:$false
    $ipToSkip=(Get-NetAdapter -Name "vEthernet (WSL)" | Get-NetIPAddress | Where-Object {$_.IPAddress -NotMatch $wsl_host_ip} | Where-Object {$_.AddressFamily -Match "IPv4"} | Where-Object {$_.SkipAsSource -Match "False"}).IPv4Address
    if ($ipToSkip -ne $nill){
        Set-NetIPAddress -IPAddress $ipToSkip -SkipAsSource:$true
    }

    Write-Host "Set wsl dns ip in hosts's /etc/hosts"
    $win_config = wsl.exe -d $wsl_distro grep 'wsl' /mnt/c/Windows/System32/drivers/etc/hosts

    If ([string]::IsNullOrEmpty($win_config)){
        wsl -d $wsl_distro -u root bash -c "echo '${wsl_ip}\t\twsl' >> /mnt/c/Windows/System32/drivers/etc/hosts"
    } Else {
        wsl -d $wsl_distro -u root bash -c "sed -i 's/.*wsl/${wsl_ip}\twsl/g' /mnt/c/Windows/System32/drivers/etc/hosts"
    }
    

    #################################
    ############ Run WSL ############
    #################################
    
    wsl.exe -d $wsl_distro bash -c "echo 'Starting WSL ...' && sleep 15"
    
    Write-Host "Forward px proxy port from wsl2 interface to host loopback interface"
    netsh interface portproxy reset
    netsh interface portproxy add v4tov4 listenaddress=$wsl_host_ip listenport=$px_port connectaddress=$host_loopback_ip connectport=$px_port

    wsl.exe
}
Catch
{
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName

    Write-Host "An error occurred: $ErrorMessage"
    Write-Host $_.ScriptStackTrace

    Read-Host -Prompt "`nPress Enter to exit"

    Break
}
