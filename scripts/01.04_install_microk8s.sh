#!/bin/bash

KUBERNETES_VERSION="1.25"

printf "\n\
==========================\n\
    INSTALLING MICROK8S\n\
==========================\n\

version: ${KUBERNETES_VERSION}
"

if [[ -z $http_proxy ]]; then
    echo "no http proxy found. moving on..."
else
    echo "setting up http proxy for snap"
    snap set system proxy.http="${http_proxy}"
fi

if [[ -z $https_proxy ]]; then
    echo "no https proxy found. moving on..."
else
    echo "setting up https proxy for snap"
    snap set system proxy.https="${https_proxy}"
fi

snap install microk8s --classic --channel=$KUBERNETES_VERSION

if [[ -n $SUDO_USER ]];then
    usermod -a -G microk8s $SUDO_USER
    chown -f -R $SUDO_USER ~/.kube
fi

# PROXY SETUP

if [[ -n $http_proxy ]]; then
    echo "http_proxy setting detected. configuring microk8s..."
    echo HTTP_PROXY="$http_proxy" >> /var/snap/microk8s/current/args/containerd-env
fi

if [[ -n $https_proxy ]]; then
    echo "https_proxy setting detected. configuring microk8s..."
    echo HTTPS_PROXY="$https_proxy" >> /var/snap/microk8s/current/args/containerd-env
fi

if [[ -n $no_proxy ]]; then
    echo "no_proxy setting detected. configuring microk8s..."
    echo NO_PROXY="$no_proxy,10.1.0.0/16,10.152.183.0/24" >> /var/snap/microk8s/current/args/containerd-env
fi

if [[ -n $http_proxy ]] | [[ -n $https_proxy ]];then
    microk8s.stop
    microk8s.start
fi
