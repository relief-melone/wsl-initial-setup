#!/bin/bash

BASEDIR=$(dirname $0)

read -p "Please enter desired username: " username

useradd -m -s /bin/bash -G sudo $username
# Reset the password of the default user
passwd $username

cp $BASEDIR/../etc/sudoers /etc/sudoers
cp $BASEDIR/../etc/wsl.conf /etc/wsl.conf
printf "default = $username \n" >> /etc/wsl.conf

export CREATED_USER=$username