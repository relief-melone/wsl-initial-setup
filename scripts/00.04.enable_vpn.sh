#!/bin/bash

BASEDIR=$(dirname $0)

printf "\n\
==========================\n\
     SETTING VPN ACCESS\n\
==========================\n\
"
powershell.exe $BASEDIR/hostmachine/enableVpnForWsl2.ps1

cat $BASEDIR/00.04.b.enable_vpn_bashrc.sh >> /root/.bashrc
cat $BASEDIR/00.04.b.enable_vpn_bashrc.sh >> /home/$CREATED_USER/.bashrc

source $BASEDIR/00.04.b.enable_vpn_bashrc.sh

whiptail --yesno "Is your VPN connection active now?" 20 50

ACTIVE_NOW=$?

if [[ $ACTIVE_NOW -eq 0 ]]; then
     vpnResolv
fi

