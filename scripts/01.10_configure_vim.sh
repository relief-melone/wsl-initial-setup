#!/bin/bash

printf "\n\
==========================\n\
     CONFIGURING VIM\n\
==========================\n\
"

BASEDIR=$(dirname $0)

if [[ -n $SUDO_USER ]]; then
     cp $BASEDIR/../home/.vimrc /home/$SUDO_USER/.vimrc
fi
cp $BASEDIR/../home/.vimrc ~/.vimrc
