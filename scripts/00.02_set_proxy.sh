#!/bin/bash

BASEDIR=$(dirname $0)


printf "\n\
==========================\n\
     SETTING PROXY\n\
==========================\n\
"

BASEDIR=$(dirname $0)
WSL_HOST_IP="169.254.254.1"

    #################################
    #####     User Prompt       #####
    #################################

# Creates a default user and adds it to the sudo group
proxy=$(whiptail --inputbox "Please enter your proxy in the format http://user:password@your-proxy.com.\n \
The default has been set to the host machines IP Address (use the setupNetwork.ps1 to make this ip availaible)" 20 50 "http://${WSL_HOST_IP}:3128" \
     3>&1 1>&2 2>&3)

noproxy_user=$(whiptail --inputbox "Please enter no_proxy addresses (standard values like 0.0.0.0 will be set automatically" 20 50 "" \
     3>&1 1>&2 2>&3)

if [[ $noproxy_user != "" ]]; then
     noproxy_user="$noproxy_user,"
fi

whiptail --yesno "are these settings correct: \n proxy: $proxy \n no_proxy: $noproxy_user" 20 50
settings_correct=$?

if [[ $settings_correct -eq 1 ]]; then
     echo "Cancelling..."
     exit 0
fi

echo "Acquire::http::Proxy \"${proxy}\";" > /etc/apt/apt.conf.d/proxy.conf
echo "Acquire::https::Proxy \"${proxy}\";" >> /etc/apt/apt.conf.d/proxy.conf


    #################################
    #####      HTTP PROXY       #####
    #################################

mkdir -p /etc/sysconfig
echo "http_proxy=${proxy}" >  /etc/environment
echo "http_proxy=${proxy}" >  /etc/sysconfig/snapd
echo "export http_proxy=${proxy}" >  /etc/environment_shell
echo "HTTP_PROXY=${proxy}" >>  /etc/environment
echo "HTTP_PROXY=${proxy}" >>  /etc/sysconfig/snapd
echo "export HTTP_PROXY=${proxy}" >>  /etc/environment_shell


     #################################
     #####      HTTPS PROXY      #####
     #################################

echo "https_proxy=${proxy}" >>  /etc/environment
echo "https_proxy=${proxy}" >>  /etc/sysconfig/snapd
echo "export https_proxy=${proxy}" >>  /etc/environment_shell
echo "HTTPS_PROXY=${proxy}" >>  /etc/environment
echo "HTTPS_PROXY=${proxy}" >>  /etc/sysconfig/snapd
echo "export HTTPS_PROXY=${proxy}" >>  /etc/environment_shell


     #################################
     #####        NO PROXY       #####
     #################################
current_proxy_ip=$(echo $proxy | awk -F'[:// ]+' '{print $2}')

noproxy="${noproxy_user}127.0.0.1,wsl.host,localhost,localhost,$current_proxy_ip,$(hostname),$(hostname |  awk '{print tolower($0)}')"
echo "no_proxy=$noproxy" >> /etc/environment
echo "no_proxy=$noproxy" >> /etc/sysconfig/snapd
echo "export no_proxy=$noproxy" >> /etc/environment_shell
echo "NO_PROXY=$noproxy" >> /etc/environment
echo "NO_PROXY=$noproxy" >> /etc/sysconfig/snapd
echo "export NO_PROXY=$noproxy" >> /etc/environment_shell

souce /etc/environment_shell


