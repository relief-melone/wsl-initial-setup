#!/bin/bash

echo "1" > /etc/rm-install/stage

echo """
    STAGE 0 COMPLETED

    BEFORE FURTHER USE PLEASE EXIT WSL AND SHUT IT DOWN USING
    
    wsl --shutdown

    REMEMBER TO RUN THE NETWORK SCRIPT FROM YOUR DESKTOP AGAIN AFTERWARDS!!

    THEN ENTER AGAIN USING THE COMMAND
    
    wsl -d [distro-name] --user [your-username]

    THEN START THIS INSTALL SCRIPT AGAIN USING

    sudo /mnt/c/path/to/install.sh
"""


exit