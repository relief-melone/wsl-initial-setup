#!/bin/bash

printf "\n\
==========================\n\
    CONFIGURING NANO\n\
==========================\n\
"

if [[ !$(command -v curl) ]]; then
  apt-get install -y curl
fi

if [[ !$(command -v unzip) ]]; then
  apt-get install -y unzip
fi

# ADD SYNTAX SUPPORT
curl https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh | sh

if [[ -n $SUDO_USER ]]; then
  curl https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh | sudo -u $SUDO_USER sh
fi


addLine="include \"~/.nano/ts.nanorc\""

grep -qxF "$addLine" ~/.nanorc || echo $addLine >> ~/.nanorc

if [[ -n $SUDO_USER ]]; then
  grep -qxF "$addLine" /home/$SUDO_USER/.nanorc || echo $addLine >> /home/$SUDO_USER/.nanorc
fi


# SET FORMATTING

formatTabsize="set tabsize 2"
formatTabsToSpaces="set tabstospaces"


grep -qxF "$formatTabsize" ~/.nanorc || echo -e $formatTabsize >> ~/.nanorc

if [[ -n $SUDO_USER ]]; then
  grep -qxF "$formatTabsize" /home/$SUDO_USER/.nanorc || echo -e $formatTabsize >> /home/$SUDO_USER/.nanorc
  grep -qxF "$formatTabsToSpaces" /home/$SUDO_USER/.nanorc || echo -e $formatTabsToSpaces >> /home/$SUDO_USER/.nanorc
fi

grep -qxF "$formatTabsToSpaces" ~/.nanorc || echo -e $formatTabsToSpaces >> ~/.nanorc
