#!/bin/bash

BASEDIR=$(dirname $0)
RUN_NEXT_STAGE=$1

setUser(){
  source $BASEDIR/00.01_set_user.sh
}

setProxy(){
  source $BASEDIR/00.02_set_proxy.sh
}

systemd(){
  source $BASEDIR/00.03_enable_systemd.sh
}

setBashRc(){
  source $BASEDIR/01.03_update_bashrc.sh
}

setUser

I_CORPORATE_TOOLS=$(whiptail --checklist "CORPORATE" 10 75 3 \
  setProxy    "Use a proxy as http and https proxy"             off \
  3>&1 1>&2 2>&3)

read -a I_CORPORATE_TOOLS <<< $I_CORPORATE_TOOLS

for component_script in "${I_CORPORATE_TOOLS[@]}"; do
  echo "RUNNING $component_script"
  eval $component_script
done

setBashRc
env | grep -i proxy

echo "Checking internet connectivity..."
$BASEDIR/helpers/checkInternetStatus.sh
internetConnection=$?


if [ "$internetConnection" -eq 0 ]; then
  I_WSL_TOOLS=$(whiptail --checklist "WSL" 10 75 3 \
    systemd     "Enable Systemd (Only WSL)"                       on \
    3>&1 1>&2 2>&3)


  read -a I_WSL_TOOLS <<< $I_WSL_TOOLS


  for component_script in "${I_WSL_TOOLS[@]}"; do
    eval $component_script
  done

  ### NEXT STAGE ###
  if [[ $RUN_NEXT_STAGE == "yes" ]]; then
    $BASEDIR/00.99_next_stage.sh
  fi

else
  echo """
  ===== NO INTERNET CONNECTION =====
  please check your network settings
  exiting script
  """
  exit 1

fi

