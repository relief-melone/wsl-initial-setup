#!/bin/bash

printf "\n\
==========================\n\
     CONFIGURING BASH\n\
==========================\n\
"

BASEDIR=$(dirname $0)

if [[ -n $SUDO_USER ]]; then
    cat $BASEDIR/../home/.bashrc > /home/$SUDO_USER/.bashrc
fi
cat $BASEDIR/../home/.bashrc > ~/.bashrc

