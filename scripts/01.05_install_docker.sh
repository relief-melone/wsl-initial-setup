#!/bin/bash

printf "\n\
==========================\n\
     INSTALLING DOCKER\n\
==========================\n\
"

if [[ !$(command -v curl) ]]; then
  apt-get install -y curl
fi

# Add User to docker group
groupadd docker
if [[ -n $SUDO_USER ]]; then
  usermod -aG docker $SUDO_USER
fi

# add Docker Key and Repo
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# update
apt-get update -y

# install docker
apt-get install -y docker-ce containerd.io

# proxy setup
if [[ -n $http_proxy ]]; then
  mkdir /etc/systemd/system/docker.service.d
  echo "[Service]" > /etc/systemd/system/docker.service.d/10_docker_proxy.conf
  echo "Environment=HTTP_PROXY=$http_proxy" >> /etc/systemd/system/docker.service.d/10_docker_proxy.conf
fi

if [[ -n $https_proxy ]]; then
  if [[ -z $http_proxy ]]; then
    mkdir /etc/systemd/system/docker.service.d
    echo "[Service]" > /etc/systemd/system/docker.service.d/10_docker_proxy.conf
  fi

  echo "Environment=HTTPS_PROXY=$https_proxy" >> /etc/systemd/system/docker.service.d/10_docker_proxy.conf
fi

if [[ -n $no_proxy ]]; then
  echo "Environment=NO_PROXY=$no_proxy" >> /etc/systemd/system/docker.service.d/10_docker_proxy.conf
fi

systemctl daemon-reload
systemctl restart docker