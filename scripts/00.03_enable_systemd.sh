#!/bin/bash

printf "\n\
==========================\n\
     ENABLING SYSTEMD\n\
==========================\n\
"

echo """
[boot]
systemd=true
""" >> /etc/wsl.conf
