#!/bin/bash

if [[ -z $(ip a l eth0 | grep "169.254.254.2") ]]; then
  echo "[WARN] Could not detect network interface 169.254.254.2. Do you need to run the network script first?"
else
  echo "[OK] Network interface 169.254.254.1 detected"
fi

if [[ -f "/etc/environment_shell" ]]; then
  source /etc/environment_shell
fi

wget -q --spider http://google.com

if [[ $? -eq 0 ]]; then
    echo "[OK] connected to internet"
    exit 0
else
    echo "[ERR] disconnected"
    exit 1
fi