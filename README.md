# My Basic Setup for WSL

Set of scripts to run on a freshly installed ubtuntu rootfs version. Current ones can be found here

https://cloud-images.ubuntu.com/focal/current/

My focus on wsl is to have a working setup to use for software-develpment. The features are listed below

## Included Features

- systemd
- powerline-go
- powerline-fonts
- microk8s
- kubectl
- helm
- skaffold
- node

## Usage

### 1. Enable WSL2 according to the Microsoft Docs

Complete steps 1-5 from microsofts documentation. You will not need to do step 6 as we'll be installing a distro manually.

https://docs.microsoft.com/de-de/windows/wsl/install-win10#manual-installation-steps


### 2. Download wsl rootfs Ubuntu

see [https://cloud-images.ubuntu.com/focal/current](https://cloud-images.ubuntu.com/focal/current) for current images. Make sure you download a wsl rootfs version so e.g. focal-server-cloudimg-amd64-wsl.rootfs.tar.gz

Example
```sh
curl https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64-wsl.rootfs.tar.gz -o focal-server-cloudimg-amd64-wsl.rootfs.tar.gz
```

### 3. Install WSL

I use a folder to keep sources and one for the distros as

- C:\wsl\distro
- C:\wsl\source

If you choose to store it somewhere else adjust folder names accordingly

```sh
wsl --import ubuntu c:\wsl\distro\ubuntu c:\wsl\source\focal-server.amd64-wsl.tar.gz
```

### 4. Install network for corporate environments (optional)

This is recommended if you are using wsl behind a proxy. We recommend you install and use [px proxy](https://github.com/genotrance/px).

If you have multiple wsl instances installed make sure you set the one you just imported as your default one

```sh
wsl --set-default ubuntu
```

This will add a static IP for your windows host machine and your wsl instance 

#### 4.0 Download px proxy

Downdload px proxy from https://github.com/genotrance/px/releases and extract the contents in a folder

#### 4.1 Create Shortcut Icon for network setup

Start ./scripts/hostmachine/createShortcutForNetworkSettings.ps1 as Admin from your Hostmachine inside Powershell

#### 4.2 Start "wsl2 network setup"
**If you are using a VPN make sure its disconnected**
Start the previously created wsl2 network setup shortcut from your desktop

#### 4.3 Run px proxy 
start px.exe. Check your windows task manager and stop all px.exe processes that might still be running before)

#### 4.4 VPN
If you are using a VPN connect again now


### 5 Start the script

Start the install script for the first time. This will install systemd. You will also get prompted to work with the px proxy.

```sh 
# On Windows
wsl

# In WSL
cd /mnt/c/path/to/wsl-initial-setup
./install.sh
```



**Proxy**
If you choose the installation of the proxy the default is configured to work with px-proxy

### 6. Start the install script again as the user

Now we can install the rest of the applications as with systemd we also get access to snap. But this time we will run as the user we created.

During the installation you might see a GRUB installation prompt. Just choose one of the sba drives and continue.

Please make sure you run the script from inside wsl

```sh
wsl --user [your-username]
# Inside of WSL
cd /mnt/c/path/to/wsl-initial-setup
sudo ./install.sh
```

### 7. Enjoy!

## Corporate Environments

If you want to use this behind a corporate proxy follow the steps under 4.

## Working with Windows Terminal

If you want to use Windows Terminal there are two profiles you can use. This is to use it as is with systemd enabled

```json
{
    "commandline": "wsl --user my-user",
    "guid": "{4875a9df-a2cd-406e-9b8c-bc1444ccead6}",
    "hidden": false,
    // "icon": "C:\\media\\my-icon.ico",
    "name": "WSL [sd]"
},
```

If you also want a profile that has the original init process use this profile

```json
{
    "commandline": "wsl --user my-user /bin/bash",
    "guid": "{47a6ab04-0d00-41a0-9c59-e74f30f32dd6}",
    "hidden": false,
    // "icon": "C:\\media\\my-icon.ico",
    "name": "WSL [oi]"
}
```

