#!/bin/bash

BASEDIR=$(dirname $0)

bash $BASEDIR/scripts/00.00_init_stage.sh

currentStage=$(cat /etc/rm-install/stage)
# echo "Current Stage is: $currentStage"


if [[ currentStage -eq 0 ]]; then
  echo """
  ===== STAGE 0 =====
  """
  $BASEDIR/scripts/_stage0.sh yes
fi

$BASEDIR/scripts/helpers/checkInternetStatus.sh
internetConnection=$?

if [ "$internetConnection" -eq 0 ]; then
  if [[ currentStage -eq 1 ]]; then
  echo """
  ===== STAGE 1 =====
  """
    $BASEDIR/scripts/_stage1.sh yes
  fi
else
 echo """
  ===== NO INTERNET CONNECTION =====
  please check your network settings
  exiting script
  """
  exit 1
fi
